package hu.pazmany.bsmarter;

import android.util.Log;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.HybridBinarizer;

import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.nio.IntBuffer;

/**
 * Created by Notice on 2016.02.16..
 */
class QRWorker implements Runnable {

    private static final String TAG = "QRWorker";
    private boolean running;
    private IntBuffer bank1;
    private IntBuffer bank2;
    private IntBuffer current;
    private int width;
    private int height;
    private volatile boolean currentIsEmpty = true;
    private OnQRCodeFoundListener onQRCodeFoundListener;

    public QRWorker(OnQRCodeFoundListener onQRCodeFoundListener) {
        this.onQRCodeFoundListener = onQRCodeFoundListener;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(TAG);
        running = true;
        while (running && !initialized()) {
            sleep();
        }
        while (running) {
            if (getWorkingBuffer().capacity() == 0) {
                sleep();
            }
            doWork(getWorkingBuffer());
            swapBank();
        }
    }

    private synchronized void sleep() {
        Log.d(TAG, "sleeping");
        try {
            wait();
        } catch (InterruptedException e) {
            // nop
        }
        Log.d(TAG, "woke up");
    }

    private void doWork(IntBuffer buffer) {
        int[] array = buffer.array();
        if (array.length > 0) {
            recognize(array, width, height);
            buffer.clear();
        } else {
            Log.d(TAG, "Nothing to do.");
        }
    }

    private synchronized void swapBank() {
        current = getWorkingBuffer();
        currentIsEmpty = true;
    }

    private IntBuffer getWorkingBuffer() {
        return current == bank1 ? bank2 : bank1;
    }

    public synchronized void put(int[] intArray) {
        if (currentIsEmpty) {
            currentIsEmpty = false;
            current.clear();
            current.put(intArray);
            this.notify();
        } else {
            Log.d(TAG, "dropping frame");
        }
    }

    public synchronized void allocate(int byteCount) {
        bank1 = IntBuffer.allocate(byteCount);
        bank2 = IntBuffer.allocate(byteCount);
        current = bank1;
        this.notify();
    }

    private void recognize(int[] data, int width, int height) {
        RGBLuminanceSource source = new RGBLuminanceSource(width, height, data);
//                    data, width, height, 0, 0, width, height, false);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        // Read QR Code
        Reader reader = new MultiFormatReader();
        Result result;
        try {
            result = reader.decode(bitmap);
            ResultPoint[] resultPoints = result.getResultPoints();
            if (resultPoints != null) {
                if (resultPoints.length >= 3) {
                    float centerX = (resultPoints[0].getX() + resultPoints[2].getX()) / 2.0f;
                    float centerY = (resultPoints[0].getY() + resultPoints[2].getY()) / 2.0f;
                    float horizontal = centerX / (float) width;
                    float vertical = centerY / (float) height;
                    String text = result.getText();
                    Log.i(TAG, String.format("Found : %s, h: %1.3f, v: %1.3f", text, horizontal, vertical));
                    onQRCodeFoundListener.onQRCodeFound(new QRHandler.QRHandlerData(text, horizontal, vertical));
                }
            }
        } catch (NotFoundException e) {
            //Log.d(TAG, "not found");
        } catch (ChecksumException e) {
            Log.d(TAG, "wriong checksum", e);
        } catch (FormatException e) {
            Log.d(TAG, "format exception", e);
        }
    }

    private Line createLine(ResultPoint start, ResultPoint end) {
        return new Line(createPoint(start), createPoint(end), Recognizer.TOLERANCE);
    }

    private Vector3D createPoint(ResultPoint resultPoint) {
        return new Vector3D(resultPoint.getX(), resultPoint.getY(), 0f);
    }

    public void stop() {
        running = false;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int capacity() {
        return bank1.capacity();
    }

    public boolean initialized() {
        return bank1 != null && bank2 != null;
    }

    public boolean currentIsEmpty() {
        return currentIsEmpty;
    }

    public interface OnQRCodeFoundListener {
        void onQRCodeFound(QRHandler.QRHandlerData data);
    }
}
