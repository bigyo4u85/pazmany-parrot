package hu.pazmany.bsmarter;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.parrot.freeflight.activities.ControlDroneActivity;
import com.parrot.freeflight.service.DroneControlService;


/**
 * Created by Notice on 2016.01.24..
 */
public class DroneControlActivity extends ControlDroneActivity implements QRWorker.OnQRCodeFoundListener {
    private static final String TAG = "DroneControlActivity";
    private DroneControlService droneControlService;
    private boolean flying;
    private Recognizer recognizer;
    private QRWorker.OnQRCodeFoundListener callback;
    private Handler handler;
    private DroneMover mover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recognizer = new Recognizer();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                QRHandler.QRHandlerData data = (QRHandler.QRHandlerData) msg.getData().getSerializable(QRHandler.QRHandlerData.class.getName());
                onQRCodeFound(data);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        callback = new QRHandler(handler);
        recognizer.start(callback);
    }

    @Override
    protected void onPause() {
        super.onPause();
        recognizer.stop();
    }

    @Override
    protected void onDroneServiceConnected() {
        super.onDroneServiceConnected();
        droneControlService = getDroneControlService();
        mover = new DroneMover(new Handler(), droneControlService);
    }

    @Override
    public void onDroneFlyingStateChanged(boolean flying) {
        super.onDroneFlyingStateChanged(flying);
        this.flying = flying;
    }

    @Override
    public void onDeviceOrientationChanged(float[] orientation, float magneticHeading, int magnetoAccuracy) {
        super.onDeviceOrientationChanged(orientation, magneticHeading, magnetoAccuracy);
    }

    @Override
    public void onUpdate(Bitmap video) {
        recognizer.updateFrame(video);
    }

    @Override
    public boolean waitingForFrame() {
        return recognizer.waitingForFrame();
    }

    @Override
    public void onQRCodeFound(QRHandler.QRHandlerData data) {
        ImageView qrPos = getHudView().getQrPos();
        ViewGroup parent = (ViewGroup) qrPos.getParent();
        qrPos.setTranslationX((data.getHorizontal() * parent.getMeasuredWidth()));
        qrPos.setTranslationY((data.getVertical() * parent.getMeasuredHeight()));
        if (!mover.moving()) {
            mover.move(data.getHorizontal(), data.getVertical());
        }
    }

    private class DroneMover {
        private static final long MOVE_TIME = 200;
        private static final long STOP_THRESHOLD = 50;
        private static final float ROLL_MULTIPLIER = 0.08f;
        private static final float PITCH_MULTIPLIER = 0.08f;
        private Handler handler;

        private boolean moving;
        private DroneControlService droneControlService;

        public DroneMover(Handler handler, DroneControlService droneControlService) {
            this.handler = handler;
            this.droneControlService = droneControlService;
        }

        public boolean moving() {
            return moving;
        }

        public void move(float horizontal, float vertical) {
            if (moving) return;
            moving = true;
            setAutoMoving(true);
            droneControlService.setProgressiveCommandEnabled(true);
            final float roll = ((horizontal * 2f) - 1f) * ROLL_MULTIPLIER;
            final float pitch = ((vertical * 2f) - 1f) * PITCH_MULTIPLIER;

            Runnable controller = new Runnable() {
                @Override
                public void run() {
                    getHudView().setControlValues(String.format("roll: %1.3f\tpitch: %1.3f", roll, pitch));
                    droneControlService.setRoll(roll);
                    droneControlService.setPitch(pitch);
                }
            };

            Runnable stabilizer = new Runnable() {
                @Override
                public void run() {
                    droneControlService.setRoll(0.0f);
                    droneControlService.setPitch(0.0f);
                    getHudView().setControlValues(String.format("roll: %1.3f\tpitch: %1.3f", 0.0f, 0.0f));
                }
            };
            Runnable stopper = new Runnable() {
                @Override
                public void run() {
                    droneControlService.setProgressiveCommandEnabled(false);
                    setAutoMoving(false);
                    moving = false;
                }
            };
            handler.post(controller);
            handler.postDelayed(stabilizer, MOVE_TIME);
            handler.postDelayed(stopper, MOVE_TIME + STOP_THRESHOLD);
        }
    }
}
