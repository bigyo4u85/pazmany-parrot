package hu.pazmany.bsmarter;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Created by Notice on 2016.02.10..
 */
public class Recognizer {

    private static final String TAG = "Recognizer";
    public static final double TOLERANCE = 1.0;
    private QRWorker worker;


    public void start(QRWorker.OnQRCodeFoundListener callback) {
        worker = new QRWorker(callback);
        new Thread(worker).start();
    }

    public void updateFrame(Bitmap video) {
        int byteCount = video.getByteCount();
        if (!worker.initialized() || worker.capacity() != byteCount) {
            Log.d(TAG, "Allocate new size: " + byteCount);
            worker.setWidth(video.getWidth());
            worker.setHeight(video.getHeight());
            worker.allocate(byteCount);
        }
        int[] intArray = new int[video.getWidth() * video.getHeight()];
        video.getPixels(intArray, 0, video.getWidth(), 0, 0, video.getWidth(), video.getHeight());
        worker.put(intArray);
    }

    public void stop() {
        worker.stop();
    }

    public boolean waitingForFrame() {
        return worker.currentIsEmpty();
    }
}
