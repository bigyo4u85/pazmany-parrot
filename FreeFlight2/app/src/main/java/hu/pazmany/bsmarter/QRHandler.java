package hu.pazmany.bsmarter;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.Serializable;

/**
 * Created by Notice on 2016.02.16..
 */
public class QRHandler implements QRWorker.OnQRCodeFoundListener {

    private final Handler handler;

    public QRHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void onQRCodeFound(QRHandlerData data) {
        Message msg = handler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putSerializable(QRHandlerData.class.getName(), data);
        msg.setData(bundle);
        handler.sendMessage(msg);
    }

    public static class QRHandlerData implements Serializable {
        private String text;
        private float horizontal;
        private float vertical;

        public QRHandlerData(String text, float horizontal, float vertical) {
            this.text = text;
            this.horizontal = horizontal;
            this.vertical = vertical;
        }

        public String getText() {
            return text;
        }

        public float getHorizontal() {
            return horizontal;
        }

        public float getVertical() {
            return vertical;
        }
    }
}
